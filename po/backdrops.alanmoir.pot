# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the backdrops.alanmoir package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: backdrops.alanmoir\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-06 13:59+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/HelpPage.qml:8
msgid "How To Set As A Background"
msgstr ""

#: ../qml/HomePage.qml:48
msgid ", please try again later."
msgstr ""

#: ../qml/PeerPicker.qml:12
msgid "Select Download Destination"
msgstr ""

#: ../qml/SettingsPage.qml:10
msgid "Settings"
msgstr ""

#: ../qml/SettingsPage.qml:11
msgid "Refresh the home page to apply changes."
msgstr ""

#: ../qml/SettingsPage.qml:42
msgid "Dark Mode"
msgstr ""

#: ../qml/SettingsPage.qml:54
msgid "Solid Button Color"
msgstr ""

#: ../qml/SettingsPage.qml:66
msgid "Portrait Only"
msgstr ""

#: ../qml/SettingsPage.qml:79
msgid "Order by Newest"
msgstr ""

#: ../qml/SettingsPage.qml:80
msgid "Default is by relevance (recommended)"
msgstr ""

#: ../qml/SettingsPage.qml:92
msgid "Include AI Art"
msgstr ""

#: ../qml/SettingsPage.qml:104
msgid "Include Anime"
msgstr ""

#: ../qml/SettingsPage.qml:116
msgid "Include People"
msgstr ""

#: backdrops.desktop.in.h:1
msgid "backdrops"
msgstr ""
