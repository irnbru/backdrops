import QtQuick 2.7
import Lomiri.Components 1.3
import Lomiri.Content 1.3

Page {
    id: peerPicker

    property string pathToImage
    property ContentTransfer activeTransfer

    header: PageHeader {
        title: i18n.tr('Select Download Destination')
    }

    ContentPeerPicker {
        id: picker
        anchors {
            top: parent.header.bottom
            topMargin: units.gu(2)
        }
        height: childrenRect.height
        width: parent.width
        showTitle: false
        contentType: ContentType.Pictures
        handler: ContentHandler.Destination

        onPeerSelected: {
            parent.activeTransfer = peer.request()
            parent.activeTransfer.stateChanged.connect(function() {
                if(parent.activeTransfer.state === ContentTransfer.InProgress) {
                    parent.activeTransfer.items = [resultComponent.createObject(parent, { 'url': pathToImage })]
                    parent.activeTransfer.state = ContentTransfer.Charged
                }
            })
        }
    }

    Component {
        id: resultComponent
        ContentItem {}
    }
}
