import QtQuick 2.7
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import 'components'
import 'app.js' as App

Page {
    id: homePage

    header: PageHeader {
        title: root.query ? root.totalImages + " results for " + root.query : "Latest"
        trailingActionBar.actions: [
            Action {
                iconName: 'settings'
                onTriggered: stack.push(Qt.resolvedUrl('SettingsPage.qml'))
            },
            Action {
                iconName: 'reset'
                onTriggered: {
                    imageModel.clear()
                    root.currentPage = 0
                    if(root.query == '') {
                        App.fetchLatest()
                    } else {
                        App.fetchImages(root.query)
                    }
                }
            },
            Action {
                iconName: 'find'
                onTriggered: {
                    PopupUtils.open(dialog)
                }
            }
        ]

        extension: CategoryScroller {}
    }

    SearchResults { id: searchResults }

    // no results / api error
    Label {
        id: errorMessage
        width: parent.width - units.gu(9)
        wrapMode: Text.Wrap
        anchors.centerIn: parent
        text: i18n.tr(root.errMsg + ", please try again later.")
        visible: root.isError
        textSize: Label.Large
    }

    ActivityIndicator {
        id: spinner
        running: imageModel.count == 0 && !root.isError
        anchors.centerIn: parent
    }

    Component {
        id: dialog
        Dialog {
            id: dialogue
            TextField {
                id: searchbox
                focus: true
                onFocusChanged: if(text != "") App.fetchImages(text)
                onAccepted: PopupUtils.close(dialogue)
            }
            Button {
                text: 'search'
                color: theme.palette.normal.focus
                onClicked: {
                    PopupUtils.close(dialogue)
                }
            }
            Button {
                text: 'cancel'
                onClicked: PopupUtils.close(dialogue)
            }
        }
    }
}
