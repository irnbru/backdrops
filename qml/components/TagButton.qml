import QtQuick 2.7
import Lomiri.Components 1.3
import '../app.js' as App

Rectangle {
    property string tag

    width: lbl.width + units.gu(3)
    height: lbl.height + units.gu(2)
    opacity: parent.buttonOpacity
    visible: tag
    color: root.solidButtons ? theme.palette.normal.background : Qt.rgba(0,0,0,0.3)
    radius: units.gu(1)
    border {
        width: units.dp(1)
        color: root.solidButtons ? theme.palette.normal.backgroundText : Qt.rgba(1,1,1,0.3)
    }
    Behavior on opacity {
        NumberAnimation {
            duration: LomiriAnimation.BriskDuration
            easing: LomiriAnimation.StandardEasing
        }
    }

    Label {
        id: lbl

        text: tag
        anchors.centerIn: parent
        color: root.solidButtons ? theme.palette.normal.backgroundText : '#fff'
        font.bold: true
        textSize: Label.Medium
        MouseArea {
            anchors.fill: parent
            enabled: tagFlow.buttonOpacity == 1 ? true : false
            onClicked: App.fetchImages(tag)
        }
    }
}
