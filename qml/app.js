// most images from this api are inappropriate women, which users may not want to see every time they open the app
// so add '-women' to the url to filter out (most) of those images
function fetchLatest() {
    var page = root.currentPage + 1
    var ratio = root.portraitOnly ? 'portrait' : 'landscape%2Cportrait'

    var xhr = new XMLHttpRequest()

    xhr.open('GET', `https://wallhaven.cc/api/v1/search?q=-women&categories=100&purity=100&ratios=${ratio}&sorting=date_added&order=desc&ai_art_filter=1&page=${page}`, true)

    xhr.send()

    xhr.onerror = function() {
        root.isError = true
    }

    xhr.onload = function() {
        if(xhr.status != 200) {
            root.isError = true
            root.errMsg = `${xhr.status}: ${xhr.statusText}`
            console.log(`Error ${xhr.status}: ${xhr.statusText}`)
        } else {
            var res = JSON.parse(xhr.responseText)
            for(var i = 0; i < res.data.length; i++) {
                imageModel.append({
                    id: res.data[i].id,
                    smallImage: res.data[i].thumbs.original,
                    mediumImage: res.data[i].thumbs.large,
                    largeImage: res.data[i].path
                })
            }

            root.currentPage = res.meta.current_page
            root.isError = false
        }
    }
}

function buildCategory() {
    var category = '100'

    if(root.includePeople && !root.includeAnime) category = '101'
    if(root.includeAnime && !root.includePeople) category = '110'
    if(root.includeAnime && root.includePeople) category = '111'

    return category
}

function fetchImages(query) {
    // clear model if it is a new searchterm
    if(query != root.query) {
        imageModel.clear()
        root.currentPage = 0
    }

    var ratio = root.portraitOnly ? 'portrait' : 'landscape%2Cportrait'
    var sorting = root.byNewest ? 'date_added' : 'relevance'
    var includeAi = root.includeAi ? '0' : '1'
    var categories = buildCategory()
    var page = root.currentPage + 1

    var xhr = new XMLHttpRequest()

    xhr.open('GET', `https://wallhaven.cc/api/v1/search?q=${query}&categories=${categories}&purity=100&ratios=${ratio}&sorting=${sorting}&order=desc&ai_art_filter=${includeAi}&page=${page}`, true)

    xhr.send()

    xhr.onerror = function() {
        root.isError = true
    }

    xhr.onload = function() {
        if(xhr.status != 200) {
            root.isError = true
            root.errMsg = `${xhr.status}: ${xhr.statusText}`
            console.log(`Error ${xhr.status}: ${xhr.statusText}`)
        } else {
            var res = JSON.parse(xhr.responseText)
            for(var i = 0; i < res.data.length; i++) {
                imageModel.append({
                    id: res.data[i].id,
                    smallImage: res.data[i].thumbs.original,
                    mediumImage: res.data[i].thumbs.large,
                    largeImage: res.data[i].path
                })
            }

            //click on image tag, need to go back one page
            if(stack.depth == 2) stack.pop()
            root.query = query
            root.currentPage = res.meta.current_page
            root.totalImages = res.meta.total
            root.lastPage = res.meta.last_page
            root.isError = false

            // stop the spinner if 0 results
            if(res.meta.total === 0) spinner.running = false
        }
    }
}

function fetchInfo(id) {
    var xhr = new XMLHttpRequest()

    xhr.open('GET', `https://wallhaven.cc/api/v1/w/${id}`)

    xhr.send()

    xhr.onerror = function() {
        console.log('request failed')
    }

    xhr.onload = function() {
        if(xhr.status != 200) {
            console.log(`Error ${xhr.status}: ${xhr.statusText}`)
        } else {
            var res = JSON.parse(xhr.responseText)

            for(var i = 0; i < res.data.tags.length; i++) {
                fullView.tags.push(res.data.tags[i].name)
            }
        }
    }
}

// dynamically create tags for image
var component, sprite
function createSpriteObjects(tagLabel) {
    component = Qt.createComponent('./components/TagButton.qml')
    sprite = component.createObject(tagFlow)
    sprite.tag = tagLabel

    if(sprite == null) console.log('Error creating object')
}
